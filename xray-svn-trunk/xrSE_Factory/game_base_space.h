#pragma once

enum EGameTypes
{
	GAME_ANY							= 0,
	GAME_SINGLE							= 1,

	GAME_DUMMY							= 255
};
