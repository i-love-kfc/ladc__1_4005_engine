// Should be inluded for BORLAND or for SDK shared bins

#ifndef xrSyncronizeH
#define xrSyncronizeH
#pragma once

#if 0
#	define PROFILE_CRITICAL_SECTIONS
#endif

#ifdef PROFILE_CRITICAL_SECTIONS
	typedef void	(*add_profile_portion_callback)	(LPCSTR id, const u64 &time);
	void XRCORE_API	set_add_profile_portion			(add_profile_portion_callback callback);

#	define STRINGIZER_HELPER(a)		#a
#	define STRINGIZER(a)			STRINGIZER_HELPER(a)
#	define CONCATENIZE_HELPER(a,b)	a##b
#	define CONCATENIZE(a,b)			CONCATENIZE_HELPER(a,b)
#	define MUTEX_PROFILE_PREFIX_ID	#mutexes/
#	define MUTEX_PROFILE_ID(a)		STRINGIZER(CONCATENIZE(MUTEX_PROFILE_PREFIX_ID,a))
#endif

// Desc: Simple wrapper for critical section
class XRCORE_API		xrCriticalSection
{
private:
	void*				pmutex;
#ifdef PROFILE_CRITICAL_SECTIONS
	LPCSTR				m_id;
#endif

public:
#ifdef PROFILE_CRITICAL_SECTIONS
    xrCriticalSection	(LPCSTR id);
#else
    xrCriticalSection	();
#endif
    ~xrCriticalSection	();

	// Code that should be accessed by one thread begins. Only one thread can enter it
    void				Enter	();
	// Code that should be accessed by one thread FINISHED. Thread that entered critical part of code should "Leave" it in same order
    void				Leave	();
	BOOL				TryEnter();
};

#endif