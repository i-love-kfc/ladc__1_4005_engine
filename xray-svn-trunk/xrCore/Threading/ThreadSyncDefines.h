// Define thread sync methods for Game bins or for SDK shared bins or for Borland

#pragma once

// Define this if compiling SDK shared bins
#if 0
	#define USE_XRCS
#endif

#if !defined __BORLANDC__ && !defined USE_XRCS
// Stuff for GAME bins


#include <mutex>
#include <atomic>

typedef std::mutex Mutex;
typedef std::recursive_mutex RMutex;

#include "Lock.hpp"

typedef Lock AccessLock; // Custom MT access protection mechanism.

#elif !defined __BORLANDC__
// Stuff for SDK shared bins (SDK makes use of xrCore, xrSE_Factory, ETOOLs and other bins)


#include <mutex>
#include <atomic>

typedef std::mutex Mutex; // Make sure SDK does not use it in code or it can FREEZE
typedef std::recursive_mutex RMutex; // Make sure SDK does not use it in code or it can FREEZE

#include "xrSyncronize.h"

typedef xrCriticalSection AccessLock; // Custom MT access protection mechanism. SDK does not work if we use "Lock" instead of "xrCriticalSection"

#else
// Stuff for BORLAND


#include "xrSyncronize.h"

typedef xrCriticalSection AccessLock; // Custom MT access protection mechanism. SDK does not work if we use "Lock" instead of "xrCriticalSection"

#endif