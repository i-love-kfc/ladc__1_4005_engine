////////////////////////////////////////////////////////////////////////////
//	Module 		: alife_monster_detail_path_manager_inline.h
//	Created 	: 01.11.2005
//  Modified 	: 22.11.2005
//	Author		: Dmitriy Iassenev
//	Description : ALife monster detail path manager class inline functions
////////////////////////////////////////////////////////////////////////////

#pragma once

IC	CALifeMonsterDetailPathManager::object_type &CALifeMonsterDetailPathManager::object	() const
{
	R_ASSERT	(m_object);
	return		(*m_object);
}

IC	void CALifeMonsterDetailPathManager::speed										(const float &speed)
{
	R_ASSERT	(_valid(speed));
	m_speed		= speed;
}

IC	const float &CALifeMonsterDetailPathManager::speed								() const
{
	R_ASSERT	(_valid(m_speed));
	return		(m_speed);
}

IC	const CALifeMonsterDetailPathManager::PATH &CALifeMonsterDetailPathManager::path	() const
{
	return		(m_path);
}

IC	const float	&CALifeMonsterDetailPathManager::walked_distance					() const
{
	R_ASSERT	(path().size() > 1);
	return		(m_walked_distance);
}
