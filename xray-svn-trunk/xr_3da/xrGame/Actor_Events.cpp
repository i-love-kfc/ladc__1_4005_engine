#include "stdafx.h"
#include "actor.h"
#include "AnomDetectorBase.h"
#include "uigamesp.h"
#include "artifact.h"
#include "inventory.h"
#include "level.h"
#include "xr_level_controller.h"
#include "FoodItem.h"
#include "ActorCondition.h"
#include "CameraFirstEye.h"
#include "holder_custom.h"
#include "UIGameCustom.h"
#include "ui/UIInventoryWnd.h"

#ifdef DEBUG
#include "PHDebug.h"
#endif
IC BOOL BE	(BOOL A, BOOL B)
{
	bool a = !!A;
	bool b = !!B;
	return a==b;
}

void CActor::OnEvent		(NET_Packet& P, u16 type)
{
	inherited::OnEvent			(P,type);
	CInventoryOwner::OnEvent	(P,type);

	u16 id;
	switch (type)
	{
	case GE_TRADE_BUY:
	case GE_OWNERSHIP_TAKE:
		{
			P.r_u16		(id);
			bool duringSpawn = !P.r_eof() && P.r_u8();
			CObject* O	= Level().Objects.net_Find	(id);
			if (!O)
			{
				Msg("! Error: No object to take/buy [%d]", id);
				break;
			}

			CFoodItem* pFood = smart_cast<CFoodItem*>(O);
			if(pFood)
				pFood->SetCurrPlace(eItemPlaceRuck);

			PIItem item_to_take = smart_cast<PIItem>(O);
			
			if (inventory().CanTakeItem(item_to_take))
			{
				O->H_SetParent(smart_cast<CObject*>(this));

				inventory().TakeItem(item_to_take, false, true, duringSpawn);

				CUIGameSP* pGameSP = NULL;
				if(CurrentGameUI())
				{
					pGameSP = smart_cast<CUIGameSP*>(CurrentGameUI());
					if (Level().CurrentViewEntity() == this)
							CurrentGameUI()->ReInitShownUI();
				};
				
				//�������� ������������� ����� � ���������
				if(pGameSP)
				{
					if(pGameSP->TopInputReceiver() == pGameSP->m_InventoryMenu)
					{
						pGameSP->m_InventoryMenu->AddItemToBag(smart_cast<CInventoryItem*>(O));
					}
				}
				
				SelectBestWeapon(O);
			} 
			else 
			{
				NET_Packet P;
				u_EventGen(P,GE_OWNERSHIP_REJECT,ID());
				P.w_u16(u16(O->ID()));
				u_EventSend(P);
			}
		}
		break;
	case GE_TRADE_SELL:
	case GE_OWNERSHIP_REJECT:
		{
			P.r_u16		(id);
			CObject* O	= Level().Objects.net_Find	(id);
			if (!O)
			{
				Msg("! Error: No object to reject/sell [%d]", id);
				break;
			}
			bool just_before_destroy	= !P.r_eof() && P.r_u8();
			O->SetTmpPreDestroy				(just_before_destroy);

			PIItem item_to_drop = smart_cast<PIItem>(O);

			if (inventory().DropItem(item_to_drop) && !O->getDestroy())
			{
				O->H_SetParent(0,just_before_destroy);
//.				feel_touch_deny(O,2000);
				Level().m_feel_deny.feel_touch_deny(O, 1000);

			}

			SelectBestWeapon(O);

			if (Level().CurrentViewEntity() == this && CurrentGameUI())
				CurrentGameUI()->ReInitShownUI();
		}
		break;
	case GE_INV_ACTION:
		{
			s32 cmd;
			P.r_s32		(cmd);
			u32 flags;
			P.r_u32		(flags);
			s32 ZoomRndSeed = P.r_s32();
			s32 ShotRndSeed = P.r_s32();
									
			if (flags & CMD_START)
			{
				if (cmd == kWPN_ZOOM)
					SetZoomRndSeed(ZoomRndSeed);
				if (cmd == kWPN_FIRE)
					SetShotRndSeed(ShotRndSeed);
				IR_OnKeyboardPress(cmd);
			}
			else
				IR_OnKeyboardRelease(cmd);
		}
		break;
	case GEG_PLAYER_WEAPON_HIDE_STATE:
		{
			u32 State		= P.r_u32();
			BOOL	Set		= !!P.r_u8();
			inventory().SetSlotsBlocked	((u16)State, !!Set);
		}break;
	case GEG_PLAYER_ATTACH_HOLDER:
		{
			u32 id = P.r_u32();
			CObject* O	= Level().Objects.net_Find	(id);
			if (!O){
				Msg("! Error: No object to attach holder [%d]", id);
				break;
			}
			VERIFY(m_holder==NULL);
			CHolderCustom*	holder = smart_cast<CHolderCustom*>(O);
			if(!holder->Engaged())	use_Holder		(holder);

		}break;
	case GEG_PLAYER_DETACH_HOLDER:
		{
			if			(!m_holder)	break;
			u32 id			= P.r_u32();
			CGameObject*	GO	= smart_cast<CGameObject*>(m_holder);
			VERIFY			(id==GO->ID());
			use_Holder		(NULL);
		}break;
	}
}

void			CActor::MoveActor		(Fvector NewPos, Fvector NewDir)
{
	Fmatrix	M = XFORM();
	M.translate(NewPos);
	r_model_yaw				= NewDir.y;
	r_torso.yaw				= NewDir.y;
	r_torso.pitch			= -NewDir.x;
	unaffected_r_torso.yaw	= r_torso.yaw;
	unaffected_r_torso.pitch= r_torso.pitch;
	unaffected_r_torso.roll	= 0;//r_torso.roll;

	r_torso_tgt_roll		= 0;
	cam_Active()->Set		(-unaffected_r_torso.yaw,unaffected_r_torso.pitch,unaffected_r_torso.roll);
	ForceTransform(M);
}