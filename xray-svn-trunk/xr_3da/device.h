#pragma once

// Note:
// ZNear - always 0.0f
// ZFar  - always 1.0f

#include "device_base.h"
#include "pure.h"
#include "../xrcore/ftimer.h"

#include "Stats.h"

#include "../xrCore/Threading/Event.hpp"

#define DEVICE_RESET_PRECACHE_FRAME_COUNT 10

#include "../Include/xrRender/FactoryPtr.h"
#include "../Include/xrRender/RenderDeviceRender.h"

// refs
class ENGINE_API CRenderDevice: public CRenderDeviceBase
{
private:
    // Main objects used for creating and rendering the 3D scene
    u32										m_dwWindowStyle;
    RECT									m_rcWindowBounds;
    RECT									m_rcWindowClient;

	CTimer									TimerMM;

	void									_Create		(LPCSTR shName);
	void									_Destroy	(BOOL	bKeepTextures);
	void									_SetupStates();
public:
	LRESULT									MsgProc		(HWND,UINT,WPARAM,LPARAM);

	u32										dwPrecacheTotal;

	float									fWidth_2, fHeight_2;

	void									OnWM_Activate(WPARAM wParam, LPARAM lParam);
public:

	IRenderDeviceRender						*m_pRender;

	BOOL									m_bNearer;
	void									SetNearer	(BOOL enabled)
	{
		if (enabled&&!m_bNearer)
		{
			m_bNearer						= TRUE;
			mProject._43					-= EPS_L;
		}
		else if (!enabled&&m_bNearer)
		{
			m_bNearer						= FALSE;
			mProject._43					+= EPS_L;
		}

		m_pRender->SetCacheXform(mView, mProject);

		//TODO: re-implement set projection
		//RCache.set_xform_project			(mProject);
	}

	void									DumpResourcesMemoryUsage() { m_pRender->ResourcesDumpMemoryUsage();}
public:
	// Registrators
	CRegistrator	<pureFrame			>			seqFrameMT;
	CRegistrator	<pureDeviceReset	>			seqDeviceReset;
	xr_vector		<fastdelegate::FastDelegate0<> >	seqParallel;
	xr_vector		<fastdelegate::FastDelegate0<> >	seqAuxThread2;

	Mutex									Aux2PoolProtection;

	CStats*									Statistic;

	Fmatrix									mInvFullTransform;


	// Copies of corresponding members. Used for synchronization.
	Fvector									vCameraPosition_saved;

	Fmatrix									mView_saved;
	Fmatrix									mProject_saved;
	Fmatrix									mFullTransform_saved;

	bool IsR2Active(void);
	bool IsR3Active(void);
	bool IsR4Active(void);
	
	CRenderDevice			()
		:
		m_pRender(0)

#ifdef PROFILE_CRITICAL_SECTIONS
		,mt_csEnter(MUTEX_PROFILE_ID(CRenderDevice::mt_csEnter))
		,mt_csLeave(MUTEX_PROFILE_ID(CRenderDevice::mt_csLeave))
#endif

	{
	    m_hWnd              = NULL;
		b_is_Active			= FALSE;
		b_is_Ready			= FALSE;
		Timer.Start			();
		m_bNearer			= FALSE;
	};

	void	Pause							(BOOL bOn, BOOL bTimer, BOOL bSound, LPCSTR reason);
	BOOL	Paused							();

	// Scene control
	void PreCache							(u32 amount, bool b_draw_loadscreen, bool b_wait_user_input);
	BOOL Begin								();
	void Clear								();
	void End								();
	void FrameMove							();
	
	void overdrawBegin						();
	void overdrawEnd						();

	// Mode control
	void DumpFlags							();
	IC	 CTimer_paused* GetTimerGlobal		()	{ return &TimerGlobal;								}
	u32	 TimerAsync							()	{ return TimerGlobal.GetElapsed_ms();				}
	u32	 TimerAsync_MMT						()	{ return TimerMM.GetElapsed_ms() +	Timer_MM_Delta; }

	// Creation & Destroying
	void ConnectToRender();
	void Create								(void);
	void Run								(void);
	void Destroy							(void);
	void Reset								(bool precache = true);

	void Initialize							(void);
	void ShutDown							(void);

public:
	void time_factor						(const float &time_factor)
	{
		Timer.time_factor		(time_factor);
		TimerGlobal.time_factor	(time_factor);
	}
	
	IC	const float &time_factor			() const
	{
		VERIFY					(Timer.time_factor() == TimerGlobal.time_factor());
		return					(Timer.time_factor());
	}

	// Multi-threading
private:

	static void AuxThread_1(void *context);
	static void AuxThread_2(void *context);

	Event syncProcessFrame; 
	Event syncFrameDone; 
	Event Aux1Exit_Sync; // Aux thread 1 exit event sync
	Event Aux2Exit_Sync; // Aux thread 2 exit event sync

public:

	Event SoundsThreadExit_Sync; // sound loading thread exit event sync

	volatile BOOL		mt_bMustExit;

	volatile BOOL		mt_bMustExit2;

	ICF		void			remove_from_seq_parallel	(const fastdelegate::FastDelegate0<> &delegate)
	{
		xr_vector<fastdelegate::FastDelegate0<> >::iterator I = std::find(
			seqParallel.begin(),
			seqParallel.end(),
			delegate
		);
		if (I != seqParallel.end())
			seqParallel.erase	(I);
	}

public:
			void __stdcall		on_idle();
			bool __stdcall		on_message(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT &result);

private:
			void					message_loop		();
virtual		void			_BCL	AddSeqFrame			( pureFrame* f, bool mt );
virtual		void			_BCL	RemoveSeqFrame		( pureFrame* f );
virtual		CStatsPhysics*	_BCL	StatPhysics			()	{ return  Statistic ;}
};

extern		ENGINE_API		CRenderDevice		Device;
extern		ENGINE_API		bool				CheckEngineFfreezing;
extern		ENGINE_API		float				VIEWPORT_NEAR;
extern		ENGINE_API		Mutex				GameSaveMutex;

extern		ENGINE_API		bool				g_bBenchmark;

typedef fastdelegate::FastDelegate0<bool>		LOADING_EVENT;
extern	ENGINE_API xr_list<LOADING_EVENT>		g_loading_events;

class ENGINE_API CLoadScreenRenderer :public pureRender
{
public:
					CLoadScreenRenderer	();
	void			start				(bool b_user_input);
	void			stop				();
	virtual void	OnRender			();

	bool			b_registered;
	bool			b_need_user_input;
};
extern ENGINE_API CLoadScreenRenderer load_screen_renderer;

