#include "stdafx.h"
#include "GameFont.h"
#pragma hdrstop

#include "../xrcdb/ISpatial.h"
#include "IGame_Persistent.h"
#include "render.h"

#include "ConstantDebug.h"

#include "../Include/xrRender/DrawUtils.h"

#include "../Include/xrRender/UIShader.h"
#include "../Include/xrRender/UIRender.h"

// stats
DECLARE_RP(Stats);

extern BOOL show_FPS_only;
extern BOOL show_engine_timers;
extern BOOL show_render_times;
extern BOOL log_render_times;
extern BOOL log_engine_times;
extern BOOL display_ram_usage;

Fvector colorok = { 206, 180, 247 };
Fvector colorbad = { 224, 114, 119 };
u32 statscolor1 = color_rgba(206, 180, 247, 255);
u32 back_color = color_rgba(20, 20, 20, 150);
Frect back_region;

BOOL g_bDisableRedText = FALSE;

FactoryPtr<IUIShader>* back_shader;

class	optimizer	{
	float	average_	;
	BOOL	enabled_	;
public:
	optimizer	()		{
		average_	= 30.f;
//		enabled_	= TRUE;
//		disable		();
		// because Engine is not exist
		enabled_	= FALSE;
	}

	BOOL	enabled	()	{ return enabled_;	}
	void	enable	()	{ if (!enabled_)	{ Engine.External.tune_resume	();	enabled_=TRUE;	}}
	void	disable	()	{ if (enabled_)		{ Engine.External.tune_pause	();	enabled_=FALSE; }}
	void	update	(float value)	{
		if (value < average_*0.7f)	{
			// 25% deviation
			enable	();
		} else {
			disable	();
		};
		average_	= 0.99f*average_ + 0.01f*value;
	};
};
static optimizer vtune;

void _draw_cam_pos(CGameFont* pFont)
{
	float sz		= pFont->GetHeight();
	pFont->SetHeightI(0.02f);
	pFont->SetColor	(0xffffffff);
	pFont->Out		(10, 600, "CAMERA POSITION:  [%3.2f,%3.2f,%3.2f]",VPUSH(Device.vCameraPosition));
	pFont->SetHeight(sz);
	pFont->OnRender	();
}


IC void DisplayWithcolor(CGameFont& targetFont, LPCSTR msg, u32 returntocolor, float value, float bad_value){
	u32 colortodisplay;
	if (value >= 0.f && value < bad_value)
	{
		float factor = value / bad_value;

		Fvector temp;
		temp.lerp(colorok, colorbad, factor);

		colortodisplay = color_rgba((u32)temp.x, (u32)temp.y, (u32)temp.z, 255);
	}
	else
	{
		colortodisplay = color_rgba((u32)colorbad.x, (u32)colorbad.y, (u32)colorbad.z, 255);
	}

	targetFont.SetColor(colortodisplay);

	targetFont.OutNext("%fms = %s", value, msg);
	targetFont.SetColor(returntocolor);
}


void DrawRect(Frect const& r, u32 color)
{
	UIRender->PushPoint(r.x1, r.y1, 0.0f, color, 0.0f, 0.0f);
	UIRender->PushPoint(r.x2, r.y1, 0.0f, color, 1.0f, 0.0f);
	UIRender->PushPoint(r.x2, r.y2, 0.0f, color, 1.0f, 1.0f);

	UIRender->PushPoint(r.x1, r.y1, 0.0f, color, 0.0f, 0.0f);
	UIRender->PushPoint(r.x2, r.y2, 0.0f, color, 1.0f, 1.0f);
	UIRender->PushPoint(r.x1, r.y2, 0.0f, color, 0.0f, 1.0f);
}


CStats::CStats()
{
	back_shader = NULL;
	pFont = 0;

	fMem_calls = 0;
	RenderDUMP_DT_Count = 0;
	Device.seqRender.Add(this, REG_PRIORITY_LOW - 1000);

	pFPSFont = 0;
}

CStats::~CStats()
{
	Device.seqRender.Remove(this);
	xr_delete(pFont);
	xr_delete(pFPSFont);
}

ENGINE_API float fActor_Lum = 0.0f; //temp debug
ENGINE_API float fAi_Lum_calc = 0.0f; //temp debug

void CStats::Show() 
{
	// Stop timers
	{
		EngineTOTAL.FrameEnd		();	
		Sheduler.FrameEnd			();	
		UpdateClient.FrameEnd		();	
		Physics.FrameEnd			();	
		ph_collision.FrameEnd		();
		ph_core.FrameEnd			();
		Animation.FrameEnd			();	
		AI_Think.FrameEnd			();
		AI_Range.FrameEnd			();
		AI_Path.FrameEnd			();
		AI_Node.FrameEnd			();
		AI_Vis.FrameEnd				();
		AI_Vis_Query.FrameEnd		();
		AI_Vis_RayTests.FrameEnd	();
		
		RenderTOTAL.FrameEnd		();
		RenderCALC.FrameEnd			();
		RenderCALC_HOM.FrameEnd		();
		RenderDUMP.FrameEnd			();	
		RenderDUMP_RT.FrameEnd		();
		RenderDUMP_SKIN.FrameEnd	();	
		RenderDUMP_Wait.FrameEnd	();	
		RenderDUMP_Wait_S.FrameEnd	();	
		RenderDUMP_HUD.FrameEnd		();	
		RenderDUMP_Glows.FrameEnd	();	
		RenderDUMP_Lights.FrameEnd	();	
		RenderDUMP_WM.FrameEnd		();	
		RenderDUMP_DT_VIS.FrameEnd	();	
		RenderDUMP_DT_Render.FrameEnd();	
		RenderDUMP_DT_Cache.FrameEnd();
		RenderDUMP_Pcalc.FrameEnd	();	
		RenderDUMP_Scalc.FrameEnd	();	
		RenderDUMP_Srender.FrameEnd	();	
		
		Sound.FrameEnd				();
		Input.FrameEnd				();
		clRAY.FrameEnd				();	
		clBOX.FrameEnd				();
		clFRUSTUM.FrameEnd			();
		
		netClient1.FrameEnd			();
		netClient2.FrameEnd			();
		netServer.FrameEnd			();

		netClientCompressor.FrameEnd();
		netServerCompressor.FrameEnd();
		
		TEST0.FrameEnd				();
		TEST1.FrameEnd				();
		TEST2.FrameEnd				();
		TEST3.FrameEnd				();

		g_SpatialSpace->stat_insert.FrameEnd		();
		g_SpatialSpace->stat_remove.FrameEnd		();
		g_SpatialSpacePhysic->stat_insert.FrameEnd	();
		g_SpatialSpacePhysic->stat_remove.FrameEnd	();
	}

	if (Device.fTimeDelta<=EPS_S)
	{
		float mem_count		= float	(Memory.stat_calls);
		if (mem_count>fMem_calls)	fMem_calls	=	mem_count;
		else						fMem_calls	=	.9f*fMem_calls + .1f*mem_count;
		Memory.stat_calls	= 0		;
	}

	CGameFont& F = *pFont;
	float		f_base_size	= 0.01f;
				F.SetHeightI	(f_base_size);

	if (vtune.enabled())	{
		float sz		= pFont->GetHeight();
		pFont->SetHeightI(0.02f);
		pFont->SetColor	(0xFFFF0000	);
		pFont->OutSet	(Device.dwWidth/2.0f+(pFont->SizeOf_("--= tune =--")/2.0f),Device.dwHeight/2.0f);
		pFont->OutNext	("--= tune =--");
		pFont->OnRender	();
		pFont->SetHeight(sz);
	};

	// Show 
	if (psDeviceFlags.test(rsStatistic) || show_FPS_only == TRUE || show_engine_timers == TRUE || show_render_times == TRUE)
	{
		if (psDeviceFlags.test(rsStatistic))
		{
			static float	r_ps = 0;
			static float	b_ps = 0;
			r_ps = .99f*r_ps + .01f*(clRAY.count / clRAY.result);
			b_ps = .99f*b_ps + .01f*(clBOX.count / clBOX.result);

			CSound_stats				snd_stat;
			::Sound->statistic(&snd_stat, 0);
			F.SetColor(0xFFFFFFFF);
			pFont->OutSet(0, 0);

			m_pRender->OutData1(F);

#ifdef FS_DEBUG
			F.OutNext	("mapped:      %d",			g_file_mapped_memory);
			F.OutSkip	();
#endif
			m_pRender->OutData3(F);
			F.OutSkip();

#define PPP(a) (100.f*float(a)/float(EngineTOTAL.result))
			F.OutNext("*** ENGINE:  %2.2fms", EngineTOTAL.result);
			F.OutNext("Memory:      %2.2fa", fMem_calls);
			F.OutNext("uClients:    %2.2fms, %2.1f%%, crow(%d)/active(%d)/total(%d)", UpdateClient.result, PPP(UpdateClient.result), UpdateClient_crows, UpdateClient_active, UpdateClient_total);
			F.OutNext("uSheduler:   %2.2fms, %2.1f%%", Sheduler.result, PPP(Sheduler.result));
			F.OutNext("uSheduler_L: %2.2fms", fShedulerLoad);
			F.OutNext("uParticles:  Qstart[%d] Qactive[%d] Qdestroy[%d]", Particles_starting, Particles_active, Particles_destroy);
			F.OutNext("spInsert:    o[%.2fms, %2.1f%%], p[%.2fms, %2.1f%%]", g_SpatialSpace->stat_insert.result, PPP(g_SpatialSpace->stat_insert.result), g_SpatialSpacePhysic->stat_insert.result, PPP(g_SpatialSpacePhysic->stat_insert.result));
			F.OutNext("spRemove:    o[%.2fms, %2.1f%%], p[%.2fms, %2.1f%%]", g_SpatialSpace->stat_remove.result, PPP(g_SpatialSpace->stat_remove.result), g_SpatialSpacePhysic->stat_remove.result, PPP(g_SpatialSpacePhysic->stat_remove.result));
			F.OutNext("Physics:     %2.2fms, %2.1f%%", Physics.result, PPP(Physics.result));
			F.OutNext("  collider:  %2.2fms", ph_collision.result);
			F.OutNext("  solver:    %2.2fms, %d", ph_core.result, ph_core.count);
			F.OutNext("aiThink:     %2.2fms, %d", AI_Think.result, AI_Think.count);
			F.OutNext("  aiRange:   %2.2fms, %d", AI_Range.result, AI_Range.count);
			F.OutNext("  aiPath:    %2.2fms, %d", AI_Path.result, AI_Path.count);
			F.OutNext("  aiNode:    %2.2fms, %d", AI_Node.result, AI_Node.count);
			F.OutNext("aiVision:    %2.2fms, %d", AI_Vis.result, AI_Vis.count);
			F.OutNext("  Query:     %2.2fms", AI_Vis_Query.result);
			F.OutNext("  RayCast:   %2.2fms", AI_Vis_RayTests.result);
			F.OutSkip();

#undef  PPP
#define PPP(a) (100.f*float(a)/float(RenderTOTAL.result))
			F.OutNext("*** RENDER:  %2.2fms", RenderTOTAL.result);
			F.OutNext("R_CALC:      %2.2fms, %2.1f%%", RenderCALC.result, PPP(RenderCALC.result));
			F.OutNext("  HOM:       %2.2fms, %d", RenderCALC_HOM.result, RenderCALC_HOM.count);
			F.OutNext("  Skeletons: %2.2fms, %d", Animation.result, Animation.count);
			F.OutNext("R_DUMP:      %2.2fms, %2.1f%%", RenderDUMP.result, PPP(RenderDUMP.result));
			F.OutNext("  Wait-L:    %2.2fms", RenderDUMP_Wait.result);
			F.OutNext("  Wait-S:    %2.2fms", RenderDUMP_Wait_S.result);
			F.OutNext("  Skinning:  %2.2fms", RenderDUMP_SKIN.result);
			F.OutNext("  DT_Vis/Cnt:%2.2fms/%d", RenderDUMP_DT_VIS.result, RenderDUMP_DT_Count);
			F.OutNext("  DT_Render: %2.2fms", RenderDUMP_DT_Render.result);
			F.OutNext("  DT_Cache:  %2.2fms", RenderDUMP_DT_Cache.result);
			F.OutNext("  Wallmarks: %2.2fms, %d/%d - %d", RenderDUMP_WM.result, RenderDUMP_WMS_Count, RenderDUMP_WMD_Count, RenderDUMP_WMT_Count);
			F.OutNext("  Glows:     %2.2fms", RenderDUMP_Glows.result);
			F.OutNext("  Lights:    %2.2fms, %d", RenderDUMP_Lights.result, RenderDUMP_Lights.count);
			F.OutNext("  RT:        %2.2fms, %d", RenderDUMP_RT.result, RenderDUMP_RT.count);
			F.OutNext("  HUD:       %2.2fms", RenderDUMP_HUD.result);
			F.OutNext("  P_calc:    %2.2fms", RenderDUMP_Pcalc.result);
			F.OutNext("  S_calc:    %2.2fms", RenderDUMP_Scalc.result);
			F.OutNext("  S_render:  %2.2fms, %d", RenderDUMP_Srender.result, RenderDUMP_Srender.count);
			F.OutSkip();
			F.OutNext("*** SOUND:   %2.2fms", Sound.result);
			F.OutNext("  TGT/SIM/E: %d/%d/%d", snd_stat._rendered, snd_stat._simulated, snd_stat._events);
			F.OutNext("  HIT/MISS:  %d/%d", snd_stat._cache_hits, snd_stat._cache_misses);
			F.OutSkip();
			F.OutNext("Input:       %2.2fms", Input.result);
			F.OutNext("clRAY:       %2.2fms, %d, %2.0fK", clRAY.result, clRAY.count, r_ps);
			F.OutNext("clBOX:       %2.2fms, %d, %2.0fK", clBOX.result, clBOX.count, b_ps);
			F.OutNext("clFRUSTUM:   %2.2fms, %d", clFRUSTUM.result, clFRUSTUM.count);
			F.OutSkip();
			F.OutNext("netClientRecv:   %2.2fms, %d", netClient1.result, netClient1.count);
			F.OutNext("netClientSend:   %2.2fms, %d", netClient2.result, netClient2.count);
			F.OutNext("netServer:   %2.2fms, %d", netServer.result, netServer.count);
			F.OutNext("netClientCompressor:   %2.2fms", netClientCompressor.result);
			F.OutNext("netServerCompressor:   %2.2fms", netServerCompressor.result);

			F.OutSkip();

			F.OutSkip();
			F.OutNext("TEST 0:      %2.2fms, %d", TEST0.result, TEST0.count);
			F.OutNext("TEST 1:      %2.2fms, %d", TEST1.result, TEST1.count);
			F.OutNext("TEST 2:      %2.2fms, %d", TEST2.result, TEST2.count);
			F.OutNext("TEST 3:      %2.2fms, %d", TEST3.result, TEST3.count);
#ifdef DEBUG_MEMORY_MANAGER
			F.OutSkip	();
			F.OutNext	("str: cmp[%3d], dock[%3d], qpc[%3d]",Memory.stat_strcmp,Memory.stat_strdock,CPU::qpc_counter);
			Memory.stat_strcmp	=	0		;
			Memory.stat_strdock	=	0		;
			CPU::qpc_counter	=	0		;
#else
			F.OutSkip();
			F.OutNext("qpc[%3d]", CPU::qpc_counter);
			CPU::qpc_counter = 0;
#endif
			F.OutSkip();
			m_pRender->OutData4(F);

			//////////////////////////////////////////////////////////////////////////
			// Renderer specific
			F.SetHeightI(f_base_size);
			F.OutSet(200, 0);
			Render->Statistics(&F);

			//////////////////////////////////////////////////////////////////////////
			// Game specific
			F.SetHeightI(f_base_size);
			F.OutSet(400, 0);
			g_pGamePersistent->Statistics(&F);

			//////////////////////////////////////////////////////////////////////////
			// process PURE STATS
			F.SetHeightI(f_base_size);
			seqStats.Process(rp_Stats);
			pFont->OnRender();
#ifdef LA_SHADERS_DEBUG
			/////////////////////////////////////////////////////////////////////////
			// shaders constants debug
			F.SetHeightI						(f_base_size);
			F.OutSet							(1500,300);
			g_pConstantsDebug->OnRender			(pFont);
			pFont->OnRender						();
#endif
		}


		// The info on the right side of the screen

		bool draw_back = show_engine_timers || show_render_times;
		bool show_display = show_FPS_only || show_engine_timers || show_render_times || display_ram_usage;

		float devicewidth = (float)Device.dwWidth;
		float deviceheight = (float)Device.dwHeight;

		if (draw_back) // draw only when needed
		{
			if (!back_shader) //init shader, if not already
			{
				back_shader = xr_new <FactoryPtr<IUIShader> >();
				(*back_shader)->create("hud\\default", "ui\\ui_console");
			}

			float posx, posx2;
			float fontstresh_f = 150 * (devicewidth / 1280) - 150;
			posx = (900.0f + fontstresh_f) * (devicewidth / 1280.f);
			posx2 = 1230.f * (devicewidth / 1280.f);

			back_region.set(posx, 0.0f, posx2, deviceheight);

			UIRender->SetShader(**back_shader);
			UIRender->StartPrimitive(6, IUIRender::ptTriList, IUIRender::pttTL);
			DrawRect(back_region, back_color);
			UIRender->FlushPrimitive();
		}

		if (show_display)
		{
			CGameFont& fpsFont = *pFPSFont;

			fpsFont.SetColor(statscolor1);
			fpsFont.SetAligment(CGameFont::alRight);

			float posx, posy;
			if (devicewidth / deviceheight > 1.5f)
			{
				posx = 1220.f * (devicewidth / 1280.f);
				posy = 10.f * (deviceheight / 768.f);
				fpsFont.SetHeightI(0.02);
			}
			else
			{
				posx = 980.f * (devicewidth / 1024.f);
				posy = 15.f * (deviceheight / 768.f);
				fpsFont.SetHeightI(0.02);
			}

			fpsFont.OutSet(posx, posy);

			//FPS Counter
			if (show_FPS_only){
				//Delay update so that displaying of value does not refrech like crazy
				if (Device.rateControlingTimer_.GetElapsed_sec()*1000.f - UpdateFPSCounterSkip > 100.f)
				{
					UpdateFPSCounterSkip = Device.rateControlingTimer_.GetElapsed_sec()*1000.f;
					iFPS = (int)fDeviceMeasuredFPS;
					fCounterTotalMinute += fDeviceMeasuredFPS / 10.f;
				}

				//Update Total/Minute and Avrege
				if (Device.rateControlingTimer_.GetElapsed_sec()*1000.f - UpdateFPSMinute > 60000.f)
				{
					UpdateFPSMinute = Device.rateControlingTimer_.GetElapsed_sec()*1000.f;
					iTotalMinute = (int)fCounterTotalMinute;
					iAvrageMinute = (int)fCounterTotalMinute / 60;
					fCounterTotalMinute = 0;
				}

				fpsFont.OutNext("%i", iFPS);
				fpsFont.OutSkip();
				fpsFont.OutNext("A/MIN %i", iAvrageMinute);
				fpsFont.OutNext("T/MIN %i", iTotalMinute);
				fpsFont.OutNext("Actor Lum %f", fActor_Lum);
				fpsFont.OutNext("Ai Lum calc %f", fAi_Lum_calc); fAi_Lum_calc = 0.0f;
			}

			//Engine timings
			if (show_engine_timers){
				fpsFont.OutSkip();
				fpsFont.OutNext("Engine timers: Frame# %u", Device.dwFrame);

				fpsFont.OutSkip();
				DisplayWithcolor(fpsFont, "Main Thread", statscolor1, MainThreadTime, 25.f);
				DisplayWithcolor(fpsFont, "Aux Thread#1", statscolor1, Thread1Time, 10.f);
				fpsFont.OutNext("%i = Main Wait Aux Counter", MainThreadSuspendCounter);
				fpsFont.OutNext("%u = Aux Thread Objects", AuxThreadObjects);

				fpsFont.OutSkip();
				DisplayWithcolor(fpsFont, "Engine", statscolor1, EngineTOTAL.result, 5.f);
				DisplayWithcolor(fpsFont, "Render", statscolor1, RenderTOTAL.result, 22.f);

				fpsFont.OutSkip();
				DisplayWithcolor(fpsFont, "UpdateCL calls", statscolor1, UpdateClient.result, 4.f);
				DisplayWithcolor(fpsFont, "Scheduler calls", statscolor1, Sheduler.result, 4.f);

				//Logging
				if (log_engine_times){
					Msg("#Engine timers: frame# %u", Device.dwFrame);
					Msg("%fms = Main Thread", MainThreadTime);
					Msg("%fms = Aux Thread#1", Thread1Time);
					Msg("%i = Main Wait Aux Counter", MainThreadSuspendCounter);
					Msg("%u = Aux Thread Objects", AuxThreadObjects);

					Msg("%2.2fms = Engine", EngineTOTAL.result);
					Msg("%2.2fms = Render", RenderTOTAL.result);

					Msg("%2.2fms UpdateCL calls", UpdateClient.result);
					Msg("%2.2fms Scheduler calls", Sheduler.result);
				}
			}

			//Render timings
			if (show_render_times){
				fpsFont.OutSkip();
				fpsFont.OutNext("Render timers: Frame# %u", Device.dwFrame);

				fpsFont.OutSkip();
				DisplayWithcolor(fpsFont, "Stage 1",			statscolor1, Stage1Time, 4.f);
				DisplayWithcolor(fpsFont, "Stage 2",			statscolor1, Stage2Time, 4.f);
				DisplayWithcolor(fpsFont, "Sync CPU GPU",		statscolor1, CPU_wait_GPUtime, 1.f);
				DisplayWithcolor(fpsFont, "Render Main",		statscolor1, RenderMainTime, 4.f);
				DisplayWithcolor(fpsFont, "PART0",				statscolor1, PART0Time, 4.f);
				DisplayWithcolor(fpsFont, "Complex Stage",		statscolor1, ComplexStage1Time, 4.f);
				DisplayWithcolor(fpsFont, "PART1",				statscolor1, PART1Time, 4.f);
				DisplayWithcolor(fpsFont, "Render HUD UI",		statscolor1, RenderHudUiTime, 4.f);
				DisplayWithcolor(fpsFont, "Wallmarks",			statscolor1, WallmarksTime, 4.f);
				DisplayWithcolor(fpsFont, "Smap Vis Solver",	statscolor1, SmapVisSolverTime, 4.f);
				DisplayWithcolor(fpsFont, "MSAA Edges",			statscolor1, MSAAEdgesTime, 4.f);
				DisplayWithcolor(fpsFont, "Wet Surfaces",		statscolor1, WetSurfacesTime, 4.f);
				DisplayWithcolor(fpsFont, "Sun",				statscolor1, SunTime, 4.f);
				DisplayWithcolor(fpsFont, "Lights Normal",		statscolor1, Lights1Time, 4.f);
				DisplayWithcolor(fpsFont, "Lights Pending",		statscolor1, Lights2Time, 4.f);
				DisplayWithcolor(fpsFont, "Target Combine",		statscolor1, TargetCombineTime, 4.f);
				DisplayWithcolor(fpsFont, "Render Total",		statscolor1, RenderTotalTime, 22.f);

				fpsFont.OutSkip();
				DisplayWithcolor(fpsFont, "Total CPU Waited GPU",	statscolor1, CPU_wait_GPUtime + RenderDUMP_Wait.result, 2.f);

				fpsFont.OutSkip();
				fpsFont.OutNext("Occlusion Debug:");
				fpsFont.OutNext("%u = Occ_pool_size", occ_pool_size);
				fpsFont.OutNext("%u = Occ_used_size", occ_used_size);
				fpsFont.OutNext("%u = Occ_freedids_size", occ_freed_ids_size);

				fpsFont.OutSkip();
				fpsFont.OutNext("Reffer to r3/r4_R_render.cpp");

				//Logging
				if (log_render_times){
					Msg("#Reffer to r3/r4_R_render.cpp Render(); frame# = %u", Device.dwFrame);
					Msg("%fms = Stage 1", Stage1Time);
					Msg("%fms = Stage 2", Stage2Time);
					Msg("%fms = Sync CPU GPU", CPU_wait_GPUtime);
					Msg("%fms = Render Main", RenderMainTime);
					Msg("%fms = PART0", PART0Time);
					Msg("%fms = Complex Stage", ComplexStage1Time);
					Msg("%fms = PART1", PART1Time);
					Msg("%fms = Render HUD UI", RenderHudUiTime);
					Msg("%fms = Wallmarks", WallmarksTime);
					Msg("%fms = Smap Vis Solver", SmapVisSolverTime);
					Msg("%fms = MSAA Edges", MSAAEdgesTime);
					Msg("%fms = WetSurfaces", WetSurfacesTime);
					Msg("%fms = Sun", SunTime);
					Msg("%fms = Lights Normal", Lights1Time);
					Msg("%fms = Lights Pending", Lights2Time);
					Msg("%fms = Target Combine", TargetCombineTime);
					Msg("%fms = Render Total", RenderTotalTime);

					Msg("%fms = Total CPU Waited GPU", CPU_wait_GPUtime + RenderDUMP_Wait.result);

					Msg("Occlusion Debug:");
					Msg("%u = Occ_Bgn_calls", occ_Begin_calls);
					Msg("%u = Occ_End_calls", occ_End_calls);
					Msg("%u = Occ_Get_calls", occ_Get_calls);
					occ_Begin_calls = occ_End_calls = occ_Get_calls = 0;
				}
			}

			//RAM usage
			if (display_ram_usage){
				fpsFont.OutSkip();
				fpsFont.OutNext("%u KB = Used RAM", GetTotalRAMConsumption() / 1024);
			}

			pFPSFont->OnRender();
		}
	};

	if (psDeviceFlags.test(rsCameraPos)){
		_draw_cam_pos(pFont);
		pFont->OnRender();
	};

	{
		EngineTOTAL.FrameStart		();	
		Sheduler.FrameStart			();	
		UpdateClient.FrameStart		();	
		Physics.FrameStart			();	
		ph_collision.FrameStart		();
		ph_core.FrameStart			();
		Animation.FrameStart		();	
		AI_Think.FrameStart			();
		AI_Range.FrameStart			();
		AI_Path.FrameStart			();
		AI_Node.FrameStart			();
		AI_Vis.FrameStart			();
		AI_Vis_Query.FrameStart		();
		AI_Vis_RayTests.FrameStart	();
		
		RenderTOTAL.FrameStart		();
		RenderCALC.FrameStart		();
		RenderCALC_HOM.FrameStart	();
		RenderDUMP.FrameStart		();	
		RenderDUMP_RT.FrameStart	();
		RenderDUMP_SKIN.FrameStart	();	
		RenderDUMP_Wait.FrameStart	();	
		RenderDUMP_Wait_S.FrameStart();	
		RenderDUMP_HUD.FrameStart	();	
		RenderDUMP_Glows.FrameStart	();	
		RenderDUMP_Lights.FrameStart();	
		RenderDUMP_WM.FrameStart	();	
		RenderDUMP_DT_VIS.FrameStart();	
		RenderDUMP_DT_Render.FrameStart();	
		RenderDUMP_DT_Cache.FrameStart();	
		RenderDUMP_Pcalc.FrameStart	();	
		RenderDUMP_Scalc.FrameStart	();	
		RenderDUMP_Srender.FrameStart();	
		
		Sound.FrameStart			();
		Input.FrameStart			();
		clRAY.FrameStart			();	
		clBOX.FrameStart			();
		clFRUSTUM.FrameStart		();
		
		netClient1.FrameStart		();
		netClient2.FrameStart		();
		netServer.FrameStart		();
		netClientCompressor.FrameStart();
		netServerCompressor.FrameStart();

		TEST0.FrameStart			();
		TEST1.FrameStart			();
		TEST2.FrameStart			();
		TEST3.FrameStart			();

		g_SpatialSpace->stat_insert.FrameStart		();
		g_SpatialSpace->stat_remove.FrameStart		();

		g_SpatialSpacePhysic->stat_insert.FrameStart();
		g_SpatialSpacePhysic->stat_remove.FrameStart();
	}
	dwSND_Played = dwSND_Allocated = 0;
	Particles_starting = Particles_active = Particles_destroy = 0;
}

void	_LogCallback				(LPCSTR string)
{
	if (string && '!'==string[0] && ' '==string[1])
		Device.Statistic->errors.push_back	(shared_str(string));
}

void CStats::OnDeviceCreate			()
{
	g_bDisableRedText				= strstr(Core.Params,"-xclsx")?TRUE:FALSE;

	pFont	= xr_new <CGameFont>("stat_font", CGameFont::fsDeviceIndependent);
	pFPSFont = xr_new <CGameFont>("hud_font_di2", CGameFont::fsDeviceIndependent);

	if(!pSettings->section_exist("evaluation")
		||!pSettings->line_exist("evaluation","line1")
		||!pSettings->line_exist("evaluation","line2")
		||!pSettings->line_exist("evaluation","line3") )
		FATAL	("CStats::OnDeviceCreate()");

	Thread2Time = -1.f;
	Thread1Time = -1.f;
	MainThreadTime = -1.f;
	MainThreadSuspendCounter = 0;

	Stage1Time = -1.f;
	Stage2Time = -1.f;
	CPU_wait_GPUtime = -1.f;
	RenderMainTime = -1.f;
	PART0Time = -1.f;
	ComplexStage1Time = -1.f;
	PART1Time = -1.f;
	RenderHudUiTime = -1.f;
	WallmarksTime = -1.f;
	SmapVisSolverTime = -1.f;
	MSAAEdgesTime = -1.f;
	WetSurfacesTime = -1.f;
	SunTime = -1.f;
	Lights1Time = -1.f;
	Lights2Time = -1.f;
	TargetCombineTime = -1.f;
	RenderTotalTime = -1.f;

	UpdateFPSCounterSkip = 0.f;
	UpdateFPSMinute = 0.f;
	fCounterTotalMinute = 0.f;
	iTotalMinute = -1;
	iAvrageMinute = -1;
	iFPS = -1;
	fDeviceMeasuredFPS	= -1.f;
	AuxThreadObjects = 0;

	occ_pool_size = 0;
	occ_used_size = 0;
	occ_freed_ids_size = 0;
	occ_Begin_calls = 0;
	occ_End_calls = 0;
	occ_Get_calls		= 0;

#ifdef LA_SHADERS_DEBUG
	g_pConstantsDebug = xr_new <CConstantsDebug>();
#endif
}

void CStats::OnDeviceDestroy		()
{
	SetLogCB(NULL);
	xr_delete	(pFont);
	xr_delete	(pFPSFont);
	xr_delete	(back_shader);
#ifdef LA_SHADERS_DEBUG
	xr_delete	(g_pConstantsDebug);
#endif
}

void CStats::OnRender				()
{

}

//for total process ram usage
#include "windows.h"
#include "psapi.h"
#pragma comment( lib, "psapi.lib" )

u64 CStats::GetTotalRAMConsumption()
{
	PROCESS_MEMORY_COUNTERS pmc;
	BOOL result = GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	u64 RAMUsed = pmc.WorkingSetSize;

	return RAMUsed;
}
