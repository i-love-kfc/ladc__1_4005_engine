#include "stdafx.h"
#include "LightTrack.h"
#include "../../include/xrRender/RenderVisual.h"
#include "../../xr_3da/xr_object.h"

#ifdef _EDITOR
#	include "igame_persistent.h"
#	include "environment.h"
#else
#	include "../../xr_3da/igame_persistent.h"
#	include "../../xr_3da/environment.h"
#endif

extern bool	pred_energy(const CROS_impl::Light& L1, const CROS_impl::Light& L2);
ENGINE_API extern float fActor_Lum; //temp monitoring
ENGINE_API extern float fAi_Lum_calc; //temp monitoring
void CROS_impl::update_ai_luminocity(IRenderable* O)
{
	CTimer time; time.Start();
	if (Device.dwFrame < ai_lum_frame) //optimization, no need to update this code each frame;
	{
		return;
	}
	ai_lum_frame = Device.dwFrame + (u32)::Random.randI(4, 10);

	//Part 1: Find out environment luminocity

	CObject* _object = dynamic_cast<CObject*>(O);

	CEnvDescriptor&	desc = *g_pGamePersistent->Environment().CurrentEnv;

	Fvector	accumulated = { desc.ambient.x, desc.ambient.y, desc.ambient.z };
	Fvector	hemi = { desc.hemi_color.x, desc.hemi_color.y, desc.hemi_color.z };
	Fvector sun_ = { desc.sun_color.x, desc.sun_color.y, desc.sun_color.z };

	hemi.mul(hemi_value);
	sun_.mul(sun_smooth);

	accumulated.add(hemi);
	accumulated.add(sun_);

	float result = _max(accumulated.x, _max(accumulated.y, accumulated.z));
	clamp(result, 0.f, 1.f);

	ai_luminocity = result;
	if (!xr_strcmp(_object->cNameSect_str(), "actor")) fActor_Lum = ai_luminocity;
	if (ai_luminocity > 0.95f) //optimization, exit calculations if lum is close to 1.0
	{
		fAi_Lum_calc += time.GetElapsed_sec()*1000.f;
		return;
	}

	//Part 2: If the result is still less then 1.0, then update light sources luminocity

	vis_data &vis = O->renderable.visual->getVisData();
	float object_radius = vis.sphere.R;
	Fvector bb_size = { object_radius, object_radius, object_radius };
	Fvector	position;
	O->renderable.xform.transform_tiny(position, vis.sphere.P);


	//Spatials update
	g_SpatialSpace->q_box(Spatials, 0, STYPE_LIGHTSOURCE, position, bb_size); //find all light sources in radius


	//Light source pool update
	//Find Lights from spatials pool
	float dt = Device.fTimeDelta;
	for (u32 i = 0; i < Spatials.size(); i++)
	{
		ISpatial* spatial = Spatials[i];
		light* source = (light*)(spatial->dcast_Light());
		R_ASSERT(source);

		float total_range = object_radius + source->range;

		if (position.distance_to(source->position) < total_range)
		{
			// Search
			bool add = true;
			for (xr_vector<Item>::iterator I = Ai_Unsorted_Items.begin(); I != Ai_Unsorted_Items.end(); I++)
				if (source == I->source)
				{
					I->frame_touched = Device.dwFrame;
					add = false;
				}

			// Register new
			if (add)
			{
				Ai_Unsorted_Items.push_back(Item());
				Item& L = Ai_Unsorted_Items.back();

				L.frame_touched = Device.dwFrame;
				L.source = source;
				L.cache.verts[0].set(0, 0, 0);
				L.cache.verts[1].set(0, 0, 0);
				L.cache.verts[2].set(0, 0, 0);
				L.test = 0.f;
				L.energy = 0.f;
			}
		}
	}

	Ai_Tracking_Lights.clear();

	//Sort only influincing lights
	for (s32 id = 0; id < s32(Ai_Unsorted_Items.size()); id++)
	{
		// remove untouched lights
		xr_vector<CROS_impl::Item>::iterator I = Ai_Unsorted_Items.begin() + id;

		if (I->frame_touched != Device.dwFrame)
		{
			Ai_Unsorted_Items.erase(I);
			id--;
			continue;
		}

		// Trace visibility
		Fvector P, D;
		float amount = 0;
		light* Ligth_to_sort = I->source;
		Fvector &LP = Ligth_to_sort->position;
		P = position;

		// point/spot
		float f = D.sub(P, LP).magnitude();

#pragma todo("need to move raycast origin point a little towards the _object, so that raycast does not hit another lithg source or the 'lamp cover' mesh")
		BOOL res = g_pGameLevel->ObjectSpace.RayTest(LP, D.div(f), f, collide::rqtBoth, &I->cache, _object);
		if (res)
			amount -= lt_dec;
		else
			amount += lt_inc;

		I->test += amount * dt;
		clamp(I->test, -.5f, 1.f);

		I->energy = .9f * I->energy + .1f * I->test;

		float E = I->energy * Ligth_to_sort->color.intensity();
		if (E > EPS)
		{
			// Select light
			Ai_Tracking_Lights.push_back(CROS_impl::Light());
			CROS_impl::Light& L = Ai_Tracking_Lights.back();

			L.source = Ligth_to_sort;
			L.color.mul_rgb(Ligth_to_sort->color, I->energy / 2);
			L.energy = I->energy / 2;

			if (!Ligth_to_sort->flags.bStatic)
			{
				L.color.mul_rgb(.5f);
				L.energy *= .5f;
			}
		}
	}

	// Sort lights by importance
	std::sort(Ai_Tracking_Lights.begin(), Ai_Tracking_Lights.end(), pred_energy);

	//Actual Light luminocity update
	for (u32 i = 0; i < Ai_Tracking_Lights.size(); i++)
	{
		Fvector lacc = { 0, 0, 0 };
		light* light_item = Ai_Tracking_Lights[i].source;

		float d = light_item->position.distance_to(position);

#pragma todo("This not very accurate, since it does not check light direction")
		float dist_koef = (1.0f - d / (light_item->range + object_radius)); // distance influence
		clamp(dist_koef, 0.0001f, 1.f);

		lacc.x += light_item->color.r * dist_koef;
		lacc.y += light_item->color.g * dist_koef;
		lacc.z += light_item->color.b * dist_koef;

		accumulated.add(lacc);

		float result = _max(accumulated.x, _max(accumulated.y, accumulated.z));
		clamp(result, 0.01f, 1.f);

		ai_luminocity = result;
		if (!xr_strcmp(_object->cNameSect_str(), "actor"))fActor_Lum = ai_luminocity;
		if (ai_luminocity > 0.95f) //optimization, exit calculations if lum is close to 1.0
		{
			fAi_Lum_calc += time.GetElapsed_sec()*1000.f;
			return;
		}
	}
	fAi_Lum_calc += time.GetElapsed_sec()*1000.f;
}