#include "stdafx.h"
#pragma hdrstop

#include "IGame_Persistent.h"

#ifndef _EDITOR
#include "environment.h"
#	include "x_ray.h"
#	include "IGame_Level.h"
#	include "XR_IOConsole.h"
#	include "Render.h"
#	include "ps_instance.h"
#	include "CustomHUD.h"
#endif

ENGINE_API	IGame_Persistent*		g_pGamePersistent	= NULL;
extern BOOL mt_texture_prefetching;

IGame_Persistent::IGame_Persistent	()
{
	RDEVICE.seqAppStart.Add			(this);
	RDEVICE.seqAppEnd.Add			(this);
	RDEVICE.seqFrame.Add			(this,REG_PRIORITY_HIGH+1);
	RDEVICE.seqAppActivate.Add		(this);
	RDEVICE.seqAppDeactivate.Add	(this);

	m_pMainMenu						= NULL;

#ifndef _EDITOR
	pEnvironment					= xr_new <CEnvironment>();

	if (g_uCommonFlags.test(CF_Prefetch_UI)){
		Msg("*Start prefetching UI textures");
		Device.m_pRender->RenderPrefetchUITextures();
	}
#endif

	render_scene					= true;
}

IGame_Persistent::~IGame_Persistent	()
{
	RDEVICE.seqFrame.Remove			(this);
	RDEVICE.seqAppStart.Remove		(this);
	RDEVICE.seqAppEnd.Remove			(this);
	RDEVICE.seqAppActivate.Remove	(this);
	RDEVICE.seqAppDeactivate.Remove	(this);
#ifndef _EDITOR
	xr_delete						(pEnvironment);

	if (g_uCommonFlags.test(CF_Prefetch_UI)){
		ReportUITxrsForPrefetching();
	}

	if (!strstr(Core.Params, "-noprefetch") && psDeviceFlags.test(rsPrefObjects)){
		ReportVisualsForPrefetching();
	}
#endif
}

void IGame_Persistent::OnAppActivate		()
{
}

void IGame_Persistent::OnAppDeactivate		()
{
}

void IGame_Persistent::OnAppStart	()
{
#ifndef _EDITOR
	Environment().load				();
#endif    
}

void IGame_Persistent::OnAppEnd		()
{
#ifndef _EDITOR
	Environment().unload			 ();
#endif    
	OnGameEnd						();

#ifndef _EDITOR
	DEL_INSTANCE					(g_hud);
#endif    
}


void IGame_Persistent::PreStart		(LPCSTR op)
{
	string256						prev_type;
	params							new_game_params;
	xr_strcpy							(prev_type,m_game_params.m_game_type);
	new_game_params.parse_cmd_line	(op);

	// change game type
	if (0!=xr_strcmp(prev_type,new_game_params.m_game_type)){
		OnGameEnd					();
	}
}
void IGame_Persistent::Start		(LPCSTR op)
{
	string256						prev_type;
	xr_strcpy							(prev_type,m_game_params.m_game_type);
	m_game_params.parse_cmd_line	(op);
	// change game type
	if ((0!=xr_strcmp(prev_type,m_game_params.m_game_type))) 
	{
		if (*m_game_params.m_game_type)
			OnGameStart					();
#ifndef _EDITOR
		if(g_hud)
			DEL_INSTANCE			(g_hud);
#endif            
	}
	else UpdateGameType();

	VERIFY							(ps_destroy.empty());
}

void IGame_Persistent::Disconnect	()
{
#ifndef _EDITOR
	// clear "need to play" particles
	destroy_particles					(true);

	if(g_hud)
			DEL_INSTANCE			(g_hud);
//.		g_hud->OnDisconnected			();
#endif
}

void IGame_Persistent::OnGameStart()
{
#ifndef _EDITOR
	g_loading_events.push_back(LOADING_EVENT(this, &IGame_Persistent::Prefetching_stage));
#endif
}

IC bool IGame_Persistent::SceneRenderingBlocked()
{
	if (!render_scene || (m_pMainMenu && m_pMainMenu->CanSkipSceneRendering()))
	{
		return true;
	}

	return false;
}

bool IGame_Persistent::Prefetching_stage()
{
#ifndef __BORLANDC__

	loading_save_timer.Start();
	loading_save_timer_started = true;

	if (strstr(Core.Params, "-noprefetch") || !psDeviceFlags.test(rsPrefObjects))	return true;

	prefetching_in_progress = true;

	pApp->LoadPhaseBegin();

	LoadTitle("st_prefetching_objects"); Msg("# Prefetching Objects and Models");

	// prefetch game objects & models
	float p_time = 1000.f*Device.GetTimerGlobal()->GetElapsed_sec();
	u64 before_memory = Device.Statistic->GetTotalRAMConsumption();

	Log("# Loading objects");
	ObjectPool.prefetch();
	Log("# Loading models");
	Render->models_Prefetch();

	s64 memory_used = (s64)Device.Statistic->GetTotalRAMConsumption() - (s64)before_memory;

	p_time = 1000.f*Device.GetTimerGlobal()->GetElapsed_sec() - p_time;
	Msg("* [prefetch_objects_and_models] Time:[%d ms]", iFloor(p_time));
	Msg("* [prefetch_objects_and_models] Memory Aquired:[%u K]", memory_used / 1024);

	Log("# Prefetching Textures");
	Device.m_pRender->ResourcesDeferredUpload(mt_texture_prefetching);

	prefetching_in_progress = false;

	pApp->LoadPhaseEnd();

#endif

	return true;
}


void IGame_Persistent::OnGameEnd	()
{
#ifndef _EDITOR
	ObjectPool.clear					();
	Render->models_Clear				(TRUE);
#endif
}

void IGame_Persistent::OnFrame		()
{
#ifndef _EDITOR

	if (pEnvironment && (!Device.Paused() || Device.dwPrecacheFrame))
		Environment().OnFrame	();


	Device.Statistic->Particles_starting= ps_needtoplay.size	();
	Device.Statistic->Particles_active	= ps_active.size		();
	Device.Statistic->Particles_destroy	= ps_destroy.size		();

	// Play req particle systems
	while (ps_needtoplay.size())
	{
		CPS_Instance*	psi		= ps_needtoplay.back	();
		ps_needtoplay.pop_back	();
		psi->Play				(false);
	}
	// Destroy inactive particle systems
	while (ps_destroy.size())
	{
//		u32 cnt					= ps_destroy.size();
		CPS_Instance*	psi		= ps_destroy.back();
		VERIFY					(psi);
		if (psi->Locked())
		{
			Log("--locked");
			break;
		}
		ps_destroy.pop_back		();
		psi->PSI_internal_delete();
	}
#endif
}

void IGame_Persistent::destroy_particles		(const bool &all_particles)
{
#ifndef _EDITOR
	ps_needtoplay.clear				();

	while (ps_destroy.size())
	{
		CPS_Instance*	psi		= ps_destroy.back	();		
		VERIFY					(psi);
		VERIFY					(!psi->Locked());
		ps_destroy.pop_back		();
		psi->PSI_internal_delete();
	}

	// delete active particles
	if (all_particles) {
		for (;!ps_active.empty();)
			(*ps_active.begin())->PSI_internal_delete	();
	}
	else {
		u32								active_size = ps_active.size();
		CPS_Instance					**I = (CPS_Instance**)_alloca(active_size*sizeof(CPS_Instance*));
		std::copy						(ps_active.begin(),ps_active.end(),I);

		struct destroy_on_game_load {
			static IC bool predicate (CPS_Instance*const& object)
			{
				return					(!object->destroy_on_game_load());
			}
		};

		CPS_Instance					**E = std::remove_if(I,I + active_size,&destroy_on_game_load::predicate);
		for ( ; I != E; ++I)
			(*I)->PSI_internal_delete	();
	}

	VERIFY								(ps_needtoplay.empty() && ps_destroy.empty() && (!all_particles || ps_active.empty()));
#endif
}

void IGame_Persistent::OnAssetsChanged()
{
#ifndef _EDITOR
	Device.m_pRender->OnAssetsChanged(); //Resources->m_textures_description.Load();
#endif    
}

#ifndef _EDITOR

void IGame_Persistent::DestroyEnvironment()
{
	Environment().OnDeviceDestroy();
	Environment().unload			();
}

void IGame_Persistent::CreateEnvironment()
{
	Environment().load				();
	Environment().OnDeviceCreate();
	Environment().bNeed_re_create_env = TRUE;
}

void IGame_Persistent::ReportUITxrsForPrefetching()
{
	if (SuggestedForPrefetchingUI.size() > 0){
		Msg("- These UI textures are suggested to be prefetched since they caused stutterings when some UI window was loading");
		Msg("- Add this list to prefetch_ui_textures.ltx (wisely)");
		for (u32 i = 0; i < SuggestedForPrefetchingUI.size(); i++){
			Msg("%s", SuggestedForPrefetchingUI[i].c_str());
		}
	}
}

void IGame_Persistent::ReportVisualsForPrefetching()
{
	if (SuggestedForPrefetchingVisuals.size() > 0){
		Msg("- These meshes are suggested to be prefetched since they caused stutterings");
		Msg("- Add this list to prefetch ltx (wisely)");
		for (u32 i = 0; i < SuggestedForPrefetchingVisuals.size(); i++){
			Msg("%s", SuggestedForPrefetchingVisuals[i].c_str());
		}
	}
}


#endif